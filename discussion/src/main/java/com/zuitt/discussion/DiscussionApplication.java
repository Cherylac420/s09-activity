package com.zuitt.discussion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DiscussionApplication {
	ArrayList<String> enrollees = new ArrayList<String>();
	public static void main(String[] args) {
		SpringApplication.run(DiscussionApplication.class, args);
	}


	@GetMapping("/enroll")
	public String enroll(@RequestParam(value = "user", defaultValue = "Joe") String user) {
		enrollees.add(user);
		return String.format("Thank you for Enrolling, %s!", user);
	}

	@GetMapping("/getEnrollees")
	public String Enrollees() {
		return String.format(String.valueOf(enrollees));
	}

	@GetMapping("/nameage")
	//localhost:8080/greeting/nameage?name=value&age=value
	public String nameage(@RequestParam(value = "name", defaultValue = "Joe") String name, @RequestParam(value = "age", defaultValue = "0") int age) {
		return String.format("Hello %s, My age is %d ", name, age);
	}

	@GetMapping("/courses/{id}")
	// "@PathVariable" allows us to extract data directly from the URL
	public String courses(@PathVariable("id") String id) {
		if(id.equals("java101")){
			return String.format("Java 101, MWF 8:00AM-11:00AM, PHP 3000.00");
		}
		else if (id.equals("sql101")){
			return String.format("SQL 101, TTH 1:00PM-04:00PM, PHP 2000.00");
		}
		else if(id.equals("javaee101")){
			return String.format("Java EE 101, MFW 1:00PM-04:00PM, PHP 3500.00");
		}
		else {
			return String.format("Course cannot be found");
		}
	}
}


